#include "euclid.h"

euclid_result expanded_euclid_algorithm(int a, int b){
    // Switching a and b, if b is greater than a
    if (a < b){
        int tmp = a;
        a = b;
        b = tmp;
    }
    euclid_result result = {.a=a, .b=b};
    if (b== 0){ //Algorithm terminantion
        result.gcd = a;
        result.s = 1;
        result.t = 0;
    }
    else{       //Normal algorithm step. Calculate new subset and start the next iteration.
                //Extract the information and return it
        result.q = a / b;
        result.r = a % b;
        euclid_result sub_result = expanded_euclid_algorithm(result.b, result.r);
        result.gcd = sub_result.gcd;
        result.s = sub_result.t;
        result.t = sub_result.s - result.q * sub_result.t;
    }
    return result;
}
