var searchData=
[
  ['square_5fmatrix_5ffree',['square_matrix_free',['../matrix_8h.html#a5cc2c9c9847ae72576ca872de55eb146',1,'matrix.c']]],
  ['square_5fmatrix_5fget',['square_matrix_get',['../matrix_8h.html#a47d3ee6dcbf8cfb6a564d50756b73211',1,'matrix.c']]],
  ['square_5fmatrix_5fget_5fdeterminant',['square_matrix_get_determinant',['../matrix_8h.html#a19b41d0547399c6e4674cfa64eac8cdf',1,'matrix.c']]],
  ['square_5fmatrix_5fget_5fdimension',['square_matrix_get_dimension',['../matrix_8h.html#a6f810f5ab8ec56a2d78bcecbcbf33ae7',1,'matrix.c']]],
  ['square_5fmatrix_5fget_5fsub_5fmatrix',['square_matrix_get_sub_matrix',['../matrix_8h.html#ac33f436eff7c72bd9c6d18598a082c82',1,'matrix.c']]],
  ['square_5fmatrix_5fnew',['square_matrix_new',['../matrix_8h.html#a0e50fdef423bb567591f4b8f65b0b60d',1,'matrix.c']]],
  ['square_5fmatrix_5fset',['square_matrix_set',['../matrix_8h.html#a6fd9fd47ba45247927aed1a146abcbc3',1,'matrix.c']]]
];
