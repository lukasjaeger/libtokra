/** \file */
#ifndef _EUCLID_H_
#define _EUCLID_H_
/** \page euclid euclid.h
 * euclid.h provides a structure to hold all results of the expanded euclid algorithm as
 * well as a recursive function for the euclid algorithm.
 */

/**
 * Contains all numbers that are returned by the euclid-algorithm. a and b are the starting parameters,
 * gcd is the greatest common divisor, s and t are the parameters for a linear combination as + bt = gct(a,b).
 * q holds the integer quotient of a / b and r holds the rest of that division.
 */
typedef struct{
    int a;
    int b;
    int q;
    int r;
    int s;
    int t;
    int gcd;
} euclid_result;

/**
 * Recursive method that executes the expanded euclid algorithm on
 * two integer numbers.
 * @param a The greater of both numbers
 * @param b The smaller of both numbers
 * @return a data object that holds all important results of the expanded euclid algorithm.
 */
euclid_result expanded_euclid_algorithm(int a, int b);

#endif