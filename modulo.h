/** \file */
#ifndef _MODULO_H_
#define _MODULO_H_

/**
 * Performs the calculation a + b mod m
 * @param a The first summand
 * @param b The second summand
 * @param modulo The value, the sum is divided modulo by
 * @return a + b mod modulo
 */
int mod_add(int a, int b, int modulo);

/**
 * Performs the calculation a * b mod m
 * @param a The first factor
 * @param b The second factor
 * @param modulo The value, the product is divided modulo by
 * @return a * b mod modulo
 */
int mod_mul(int a, int b, int modulo);

/**
 * Performs the calculation (g^e) mod m
 * @param g The base
 * @param e The exponent
 * @param modulo The value, the result is divided modulo by
 * @return (g^e) mod modulo
 */
int mod_exp(int g, int e, int modulo);

/**
 * Performs the calculation a -b mod m
 * @param a The minuend
 * @param b The subtrahend
 * @param modulo The value, the result is divided modulo by
 * @return (a - b) mod modulo
 */
int mod_sub(int a, int b, int modulo);

/**
 * Checks whether or not a number is prime
 * param i The integer that is to be checked for prime-ness
 * return 1 if i is prime and zero if it is not or 0
 */
char is_prime(int i);

/**
 * Gets the multiplicative inverse of a number a modulo another number modulo.
 * Modulo must be greater than 0 and prime. a must be smaller than modulo.
 * @param a The number to get the multiplicative inverse for
 * @param modulo The modulus. Must be prime and modulo
 * @return -3 if modulus is negative, -2 if modulus is not prime, -1 if a >= modulus
 * and the multiplicative inverse otherwise.
 */
int get_multiplicative_inverse(int a, int modulo);
#endif
