/**\file*/
#ifndef _MATRIX_H_
#define _MATRIX_H_

/**\page matrices Matrices
 * Libtokra holds tools to work with square matrices. 
 */

/**
 * Models an a,b-matrix with a=b. The matrix holds "private" fields
 * that should not be accessed directly. Use boundary checked accessors
 * to avoid memory problems.
 */
typedef struct{
	/**
     * The matrix' height and width
	 */
	unsigned int private_dimension;
	
	/**
	 * The matrix itself
     */
	int *private_matrix;
}square_matrix;

/**
 * Creator method for a square matrix. Needs the dimension as a parameter. Every
 * number in the matrix is initialized with 0 and needs to be set.
 * @param dimension The width and height of a matrix
 * @return a new dimension * dimension - matrix
 */
square_matrix* square_matrix_new(unsigned int dimension);

/**
 * Destructor method for the matrix. Don't use ordinary free() because it leaves
 * the matrix' array in place.
 * @param matrix The matrix to de-allocate
 */
void square_matrix_free(square_matrix* matrix);

/**
 * Boundary-checked setter.
 * @param matrix The matrix to set a number in
 * @param value The value to set
 * @param i The i-coordinate of the value
 * @param j The j-coordinate of the value
 * @return 0 If set could be executed, -1 if i >= dimension and -2 if j >= dimension
 */
int square_matrix_set(square_matrix* matrix, int value, unsigned int i, unsigned int j);

/**
 * Boundary checked getter. Keep in mind that 0 can be the actual value and the
 * value you get, when i or j exceed the dimensions of the matrix.
 * @param matrix The matrix to get a value from
 * @param i The i-coordinate of the value to get
 * @param j The j-coordinate of the value to get
 * @return 0, if the dimensions of the matrix were exceeded by any parameter
 */
int square_matrix_get(square_matrix* matrix, unsigned int i, unsigned int j);

/**
 * Gets the dimension of the matrix.
 * @param matrix The matrix to get the dimension from
 * @return The dimension of matrix
 */
unsigned int square_matrix_get_dimension(square_matrix* matrix);

/**
 * Calculates the determinant of a square matrix.
 * @param matrix The matrix to calculate the determinant for
 * @return The determinant of the matrix
 */
int square_matrix_get_determinant(square_matrix* matrix);

/**
 * Creates a submatrix that evicts column number i and line number j.
 * Don't forget to free the submatrix after use.
 * @param matrix The matrix to get a sub-matrix from
 * @param i The number of the column to evict
 * @param j The number of the line to evict
 * @return The submatrix without line j and column i
 */c
square_matrix* square_matrix_get_sub_matrix(square_matrix* matrix, unsigned int i, unsigned int j);

#endif
