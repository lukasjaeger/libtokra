#include "modulo.h"
#include "euclid.h"

int mod_add(int a, int b, int modulo){
    int sum = a + b;
    while (sum < 0) sum += modulo;
    return sum % modulo;
}

int mod_mul(int a, int b, int modulo){
    int product = a * b;
    while (product < 0) product += modulo;
    return product % modulo;
}

int mod_sub(int a, int b, int modulo){
    int result = a - b;
    while (result < 0) result +=modulo;
    return result % modulo;
}

int mod_exp(int g, int e, int modulo){
    int r = 1;
    int b = g;
    while (e > 0){
        if (e % 2){
            r = mod_mul(r,b, modulo);
            e = e - 1;
        }
        b = mod_mul(b, b, modulo);
        e = e / 2;
    }
    return r;
}

char is_prime(int i){
	if (i < 1) return 0; //Out of range
	// some runtime-cutting checks	
	if (i == 1) return 1;
	if (i == 2) return 1;
	if (i == 3) return 1;
	if (i % 2 == 0) return 0;
	int upperBorder ;
	for (int j = 3; j < i; j +=2){
		upperBorder = i / j;
		if (j > upperBorder) return 1;
		if (i % j == 0) return 0;		
	}
	return 1;
}

int get_multiplicative_inverse(int a, int modulo){
	if (modulo < 1) return -3;
	if (is_prime(modulo) == 0) return -2;
	if (a >= modulo) return -1;
	euclid_result euclid = expanded_euclid_algorithm(modulo, a);
	while (euclid.t < 0) euclid.t += modulo;
	return euclid.t;
}
