#include <glib.h>
#include "matrix.h"
#include <stdio.h>

void check_matrix_1(void* ign1, gconstpointer ign2){
	square_matrix* m1 = square_matrix_new(3);
	g_assert(square_matrix_get_dimension(m1)==3);
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			g_assert(square_matrix_get(m1, i, j) == 0);
		}
	}
	g_assert(square_matrix_set(m1, 1,0,1)==0);
	g_assert(square_matrix_set(m1, 2,0,2)==0);
	g_assert(square_matrix_set(m1, 3,1,0)==0);
	g_assert(square_matrix_set(m1, 2,1,1)==0);
	g_assert(square_matrix_set(m1, 1,1,2)==0);
	g_assert(square_matrix_set(m1, 1,2,0)==0);
	g_assert(square_matrix_set(m1, 1,2,1)==0);
	g_assert(square_matrix_set(m1, 0,2,2)==0);
	
	g_assert(square_matrix_get(m1, 0,0)==0);
	g_assert(square_matrix_get(m1, 0,1)==1);
	g_assert(square_matrix_get(m1, 0,2)==2);
	g_assert(square_matrix_get(m1, 1,0)==3);
	g_assert(square_matrix_get(m1, 1,1)==2);
	g_assert(square_matrix_get(m1, 1,2)==1);
	g_assert(square_matrix_get(m1, 2,0)==1);
	g_assert(square_matrix_get(m1, 2,1)==1);
	g_assert(square_matrix_get(m1, 2,2)==0);

	g_assert(square_matrix_set(m1, 5, 3, 1) ==-1);
	g_assert(square_matrix_set(m1, 5, 1, 3)== -2);

	g_assert(square_matrix_get(m1, 3,3)==0);
	square_matrix_free(m1);
}

void check_matrix_get_determinant1(void* ign1, gconstpointer ign2){

	square_matrix* m2 = square_matrix_new(2);
	square_matrix_set(m2, 1, 0, 0);
	square_matrix_set(m2, 2, 0, 1);
	square_matrix_set(m2, 3, 1, 0);
	square_matrix_set(m2, 4, 1, 1);
	g_assert(square_matrix_get_determinant(m2) == -2);

	square_matrix* m1 = square_matrix_new(3);
	g_assert(square_matrix_set(m1, 1,0,1)==0);
	g_assert(square_matrix_set(m1, 2,0,2)==0);
	g_assert(square_matrix_set(m1, 3,1,0)==0);
	g_assert(square_matrix_set(m1, 2,1,1)==0);
	g_assert(square_matrix_set(m1, 1,1,2)==0);
	g_assert(square_matrix_set(m1, 1,2,0)==0);
	g_assert(square_matrix_set(m1, 1,2,1)==0);
	g_assert(square_matrix_set(m1, 0,2,2)==0);

	g_assert(square_matrix_get_determinant(m1) == 3);

	square_matrix_free(m2);
	square_matrix_free(m1);
}

void check_matrix_get_determinant2(void* ign1, gconstpointer ign2){
	square_matrix* m1 = square_matrix_new(3);
	g_assert(square_matrix_set(m1, 3,0,0)==0);	
	g_assert(square_matrix_set(m1, 5,0,1)==0);
	g_assert(square_matrix_set(m1, 1,0,2)==0);
	g_assert(square_matrix_set(m1, 0,1,0)==0);
	g_assert(square_matrix_set(m1, 0,1,1)==0);
	g_assert(square_matrix_set(m1, 2,1,2)==0);
	g_assert(square_matrix_set(m1, 0,2,0)==0);
	g_assert(square_matrix_set(m1, 7,2,1)==0);
	g_assert(square_matrix_set(m1, 7,2,2)==0);

	g_assert(square_matrix_get_determinant(m1) == -42);
	square_matrix_free(m1);
}
void check_matrix_get_submatrix(void* ign1, gconstpointer ign2){
	square_matrix* m1 = square_matrix_new(3);
	g_assert(square_matrix_set(m1, 1,0,1)==0);
	g_assert(square_matrix_set(m1, 2,0,2)==0);
	g_assert(square_matrix_set(m1, 3,1,0)==0);
	g_assert(square_matrix_set(m1, 2,1,1)==0);
	g_assert(square_matrix_set(m1, 1,1,2)==0);
	g_assert(square_matrix_set(m1, 1,2,0)==0);
	g_assert(square_matrix_set(m1, 1,2,1)==0);
	g_assert(square_matrix_set(m1, 0,2,2)==0);

	square_matrix* m2 = square_matrix_get_sub_matrix(m1, 0, 0);
	g_assert(square_matrix_get_dimension(m2) == 2);
	g_assert(square_matrix_get(m2, 0, 0) == 2);
	g_assert(square_matrix_get(m2, 0, 1) == 1);
	g_assert(square_matrix_get(m2, 1, 0) == 1);
	g_assert(square_matrix_get(m2, 1, 1) == 0);

	square_matrix* m3 = square_matrix_get_sub_matrix(m1, 1, 1);
	g_assert(square_matrix_get_dimension(m3) == 2);
	g_assert(square_matrix_get(m3, 0, 0) == 0);
	g_assert(square_matrix_get(m3, 0, 1) == 2);
	g_assert(square_matrix_get(m3, 1, 0) == 1);
	g_assert(square_matrix_get(m3, 1, 1) == 0);

	square_matrix* m4 = square_matrix_get_sub_matrix(m1, 2,2);
	g_assert(square_matrix_get_dimension(m4) == 2);
	g_assert(square_matrix_get(m4, 0, 0) == 0);
	g_assert(square_matrix_get(m4, 0, 1) == 1);
	g_assert(square_matrix_get(m4, 1, 0) == 3);
	g_assert(square_matrix_get(m4, 1, 1) == 2);

	square_matrix_free(m3);
	square_matrix_free(m2);
	square_matrix_free(m1);
}

int main(int argc, char** argv){
    g_test_init(&argc, &argv, NULL);
    g_test_add("/set1/test1", void, NULL, NULL, check_matrix_1, NULL);
	g_test_add("/set1/test2", void, NULL, NULL, check_matrix_get_determinant1, NULL);
	g_test_add("/set1/test3", void, NULL, NULL, check_matrix_get_submatrix,NULL);
	return g_test_run();
}
