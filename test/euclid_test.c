#include <glib.h>
#include "euclid.h"

void check_euclid_1(void* ign1, gconstpointer ign2){
    euclid_result result = expanded_euclid_algorithm(99, 78);
    g_assert(result.a == 99);
    g_assert(result.b == 78);
    g_assert(result.q == 1);
    g_assert(result.r == 21);
    g_assert(result.s == -11);
    g_assert(result.t == 14);
    g_assert(result.gcd == 3);
}

int main(int argc, char** argv){
    g_test_init(&argc, &argv, NULL);
    g_test_add("/set1/test1", void, NULL, NULL, check_euclid_1, NULL);
    return g_test_run();
}