#include <glib.h>
#include "modulo.h"
#include <stdio.h> //DEBUG

void check_modulo_add(void* ign1, gconstpointer ign2){
    g_assert(mod_add(9,4,10)==3);
}

void check_modulo_mul(void* ign1, gconstpointer ign2){
    g_assert(mod_mul(9,4,10)==6);
    g_assert(mod_mul(9,-2,10)==2);
}

void check_modulo_exp(void* ign1, gconstpointer ign2){
    g_assert(mod_exp(6, 73, 100) == 16);
}

void check_modulo_sub(void* ign1, gconstpointer ign2){
    g_assert(mod_sub(4,9,11)==6);
}

void check_is_prime(void* ign1, gconstpointer ign2){
	g_assert(is_prime (-1) == 0);
	g_assert(is_prime (0) == 0);
	g_assert(is_prime (1) == 1);
	g_assert(is_prime (2) == 1);
	g_assert(is_prime (3) == 1);
	g_assert(is_prime (4) == 0);
	g_assert(is_prime (5) == 1);
	g_assert(is_prime (6) == 0);
	g_assert(is_prime (7) == 1);
	g_assert(is_prime (8) == 0);
	g_assert(is_prime (9) == 0);
	g_assert(is_prime (10) == 0);
	g_assert(is_prime (11) == 1);
	g_assert(is_prime (12) == 0);
	g_assert(is_prime (13) == 1);
	g_assert(is_prime (14) == 0);
	g_assert(is_prime (15) == 0);
	g_assert(is_prime (16) == 0);
	g_assert(is_prime (17) == 1);
	g_assert(is_prime (18) == 0);
	g_assert(is_prime (19) == 1);
	g_assert(is_prime (20) == 0);
	g_assert(is_prime (21) == 0);
	g_assert(is_prime (22) == 0);
	g_assert(is_prime (23) == 1);
	g_assert(is_prime (24) == 0);
	g_assert(is_prime (25) == 0);
	g_assert(is_prime (26) == 0);
	g_assert(is_prime (27) == 0);
	g_assert(is_prime (28) == 0);
	g_assert(is_prime (29) == 1);
	g_assert(is_prime (30) == 0);
	g_assert(is_prime (31) == 1);
	g_assert(is_prime (32) == 0);
	g_assert(is_prime (33) == 0);
	g_assert(is_prime (34) == 0);

}

void check_get_multiplicative_inverse(void* ign1, gconstpointer ign2){
	g_assert (get_multiplicative_inverse(-3,-1) == -3);
	g_assert(get_multiplicative_inverse(3,4)==-2);
	g_assert(get_multiplicative_inverse(6,5)==-1);
	g_assert(get_multiplicative_inverse(5,5)==-1);
	g_assert(get_multiplicative_inverse(1,5)==1);
	g_assert(get_multiplicative_inverse(2,5)==3);
	g_assert(get_multiplicative_inverse(3,5)==2);
	g_assert(get_multiplicative_inverse(4,5)==4); 
}

int main(int argc, char** argv){
    g_test_init(&argc, &argv, NULL);
    g_test_add("/set1/test1", void, NULL, NULL, check_modulo_add, NULL);
    g_test_add("/set1/test2", void, NULL, NULL, check_modulo_mul, NULL);
    g_test_add("/set1/test3", void, NULL, NULL, check_modulo_exp, NULL);
    g_test_add("/set1/test4", void, NULL, NULL, check_modulo_sub, NULL);
	g_test_add("/set1/test5", void, NULL, NULL, check_is_prime, NULL);
	g_test_add("/set1/test6", void, NULL, NULL, check_get_multiplicative_inverse, NULL);
    return g_test_run();
}
