#!/bin/bash
rm euclid_test
rm modulo_test
rm matrix_test
cd ../
make
cd test
clang -Wall -std=c11 -I../ `pkg-config --cflags --libs glib-2.0` ../euclid.o euclid_test.c -o euclid_test
clang -Wall -std=c11 -I../ `pkg-config --cflags --libs glib-2.0` ../euclid.o ../modulo.o modulo_test.c -o modulo_test
clang -Wall -std=c11 -I../ `pkg-config --cflags --libs glib-2.0` ../matrix.o matrix_test.c -o matrix_test
gtester euclid_test
gtester modulo_test
gtester matrix_test
