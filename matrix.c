#include "matrix.h"
#include <stdlib.h>
#include <stdio.h>

square_matrix* square_matrix_new(unsigned int dimension){
	int* m = malloc (sizeof(int) * (dimension * dimension + 1));
	for (int i = 0; i < dimension; i++){
		for (int j = 0; j < dimension; j++){
			m[i*dimension + j] = 0;
		}
	}
	square_matrix* matrix = malloc(sizeof(square_matrix));
	matrix->private_dimension = dimension;
	matrix->private_matrix = m;
	return matrix;
}

void square_matrix_free(square_matrix* matrix){
	free(matrix->private_matrix);
	free(matrix);
}

int square_matrix_set(square_matrix* matrix, int value, unsigned int i, unsigned int j){
	if (i >= matrix->private_dimension) return -1;
	if (j >= matrix->private_dimension) return -2;
	matrix->private_matrix[i* matrix->private_dimension + j] = value;
	return 0;
}

int square_matrix_get(square_matrix* matrix, unsigned int i,unsigned int j){
	if (i >= matrix->private_dimension | j >= matrix->private_dimension) return 0;
	else return matrix->private_matrix[i* matrix->private_dimension + j];
}

unsigned int square_matrix_get_dimension(square_matrix* matrix){
	return matrix->private_dimension;
}

int square_matrix_get_determinant(square_matrix* matrix){
	int dimension = square_matrix_get_dimension(matrix);
	if (dimension == 0) return 0;
	else if (dimension == 1) return square_matrix_get(matrix, 0,0);
	else if (dimension == 2){
		int a = square_matrix_get(matrix, 0, 0);
		int b = square_matrix_get(matrix, 0, 1);
		int c = square_matrix_get(matrix, 1, 0);
		int d = square_matrix_get(matrix, 1, 1);
		return a*d - b*c;
	}
	else{
		int result = 0;
		for (int i = 0; i < dimension; i++){
			int subresult = square_matrix_get(matrix, i, 0);
			square_matrix* submatrix = square_matrix_get_sub_matrix(matrix, i, 0);
			subresult *= square_matrix_get_determinant(submatrix);
			square_matrix_free(submatrix);
			if (i % 2) subresult *= -1;
			result += subresult;
		}
		return result;
	}
}

square_matrix* square_matrix_get_sub_matrix(square_matrix* matrix, unsigned int i, unsigned int j){
	int dimension = square_matrix_get_dimension(matrix);	
	if (i >= dimension || j >= dimension) return NULL;	
	square_matrix* result = square_matrix_new(square_matrix_get_dimension(matrix) - 1);
	for (int x = 0; x< i; x++){
		for (int y = 0; y < j; y++){
			square_matrix_set(result, square_matrix_get(matrix,x,y),x,y);
		}
	}
	for (int x = 0; x < i; x++){
		for (int y = j + 1; y < dimension; y++){
			square_matrix_set(result, square_matrix_get(matrix,x,y),x,y-1);
		}
	}
	for (int x = i + 1; x < dimension; x++){
		for (int y = 0; y < i; y++){
			square_matrix_set(result, square_matrix_get(matrix,x,y),x-1,y);
		}
	}
	for (int x = i + 1; x < dimension; x++){
		for (int y = j + 1; y < dimension; y++){
			square_matrix_set(result, square_matrix_get(matrix,x,y),x-1,y-1);
		}
	}
	return result;
}
